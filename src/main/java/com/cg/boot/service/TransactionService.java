package com.cg.boot.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.boot.entity.Transaction;
import com.cg.boot.repositories.TransactionRepository;

@Service
public class TransactionService {
	
    @Autowired
    private TransactionRepository transactionRepository;

    public List<Transaction> findTransactions(String currencyFrom, String currencyTo) {
        return transactionRepository.findByCurrencyFromAndCurrencyTo(currencyFrom, currencyTo);
    }

    @Transactional
    public void saveTransaction(Transaction transaction) {
        transactionRepository.save(transaction);
    }
   
}