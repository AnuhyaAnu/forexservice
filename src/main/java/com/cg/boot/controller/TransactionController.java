package com.cg.boot.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cg.boot.entity.Transaction;
import com.cg.boot.service.TransactionService;

@RestController
@RequestMapping("/transactions")
public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    @GetMapping
    public List<Transaction> getTransactions(@RequestParam String currencyFrom, @RequestParam String currencyTo) {
        return transactionService.findTransactions(currencyFrom, currencyTo);
    }

    @PostMapping
    public void createTransaction(@Valid @RequestBody Transaction transaction) {
        transactionService.saveTransaction(transaction);
    }
    
}
