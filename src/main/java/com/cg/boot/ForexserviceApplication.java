package com.cg.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForexserviceApplication {
	
	
	    public static void main(String[] args) {
	        SpringApplication.run(ForexserviceApplication.class, args);
	        System.out.println("Spring Application Running...");
	    }
	}


